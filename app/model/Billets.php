<?php
require('vendor/autoload.php');

class Billets extends \Illuminate\Database\Eloquent\Model{
	protected $table='blog_billets';
	protected $primaryKey='id';
	public $timestamps=false;

	public function utilisateurs() {
		return $this->belongsTo('\model\Utilisateurs','id');
	}
	public function commentaires() {
		return $this->hasMany('\model\Commentaires','id');
	}	
	public function categories(){
		return $this->belongsTo('\model\Categories','id');
	}
}
?>
