<?php
require('vendor/autoload.php');

class Categories extends \Illuminate\Database\Eloquent\Model{
		protected $table='blog_categories';
		protected $primaryKey='id';
		public $timestamps=false;
		
		public function billets() {
			return $this->hasMany('\model\Billets','id');
		}
	}
?>
