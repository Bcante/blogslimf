<?php
require('vendor/autoload.php');

class Commentaires extends \Illuminate\Database\Eloquent\Model{
		protected $table='blog_commentaires';
		protected $primaryKey='id';
		public $timestamps=false; // nécessite de créer les colonnes updated at, created at
	
	public function utilisateurs(){
		return $this->belongsTo('\model\Utilisateurs','id');
	}
	public function billets() {
		return $this->belongsTo('\model\Billets','id');
	}
}
	
?>
