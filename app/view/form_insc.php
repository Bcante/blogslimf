<h1>Inscription nouveau membre</h1>
<h4>Merci de renseigner un mot de passe d'au moins 8 caractères, avec un chiffre et une lettre majuscule.</h4>
<h4>Merci de ne pas laisser de champ vide.</h4>

    <form method="post" id="nvMembre" action="inscriptione2"> <!-- Renvoie vers la route inscriptione2 -->
	<label for="name_id">Nom</label>
	<input type="text" id="pseudo_id" name="pseudo" value=""
	       placeholder="Entrez votre pseudo..." />
	<input type="text" id="nom_id" name="nom" value=""
	       placeholder="Entrez votre nom" />
	<input type="text" id="prenom_id" name="prenom" value=""
	       placeholder="Entrez votre prénom" />
	<input type="email" id="mail_id" name="mail" value=""
	       placeholder="Entrez votre mail" />
	<input type="password" id="mdp_id" name="mdp" value=""
	       placeholder="Entrez votre mdp..." />
	                     
	<input type="submit" name="sumbitInsc" value="Soumettre" />
    </form>
