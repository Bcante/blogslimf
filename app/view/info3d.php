 <?php
	$i = 1;
	echo '<div class ="infoPie">';
	
	foreach($donnesCategories as $cle => $element) {
		echo '	<p id ="label'.$i.'">'.$cle.'<p> <p id ="qte'.$i.'">'.$element.'</p>'."\n";
		$i++;
	}
	echo '	<p id ="taille">'.sizeof($donnesCategories).'</p>';
	echo '</div>';
 ?>
 
 <script src="https://code.jquery.com/jquery-1.10.0.min.js"></script>
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
	var catData = new Array();
	var nbCategorie = document.getElementById('taille').innerHTML;
	nbCategorie = Number.parseInt(nbCategorie);	
	for (var i = 1 ; i<=nbCategorie ;i++) {
		var labelTmp = 'label'+i;
		var qteTmp = 'qte'+i;
		var innerLabel = document.getElementById(labelTmp).innerHTML;
		var innerQte = document.getElementById(qteTmp).innerHTML
		//console.log("Nos variables valent: "+innerLabel+" & "+innerQte);
		var coupleData = {lab:innerLabel, qte:innerQte};
		$('.infoPie').hide();
		//var myClass = document.getElementsByClassName("infoPie");
		//myClass.removeChild(myClass);
		
		
		catData.push(coupleData);
	}
	catData.push(["Cat","Nombre"]);
	console.log(catData); 
	  
	  google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      
      function drawChart() {
        var data = new google.visualization.DataTable();
        
        data.addColumn('string','Nom catégorie');
        data.addColumn('number','Nombre de billets');
        data.addRows(nbCategorie);
        
        for (var i  = 0 ; i < nbCategorie ; i++) {
			data.setCell(i,0,catData[i].lab);
			data.setCell(i,1,catData[i].qte	); 
		}
       
		
        var options = {
          title: 'Mes catégories',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
      
      
    </script>
    
    
    <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
 </body>
