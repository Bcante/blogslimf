<?php
use conf\Authentication;
use app\model\Utilisateurs;
// Pas de session start?
class UtilisateurController extends Controller {
	
	 public function deconnexion() {
		$app = Controller::$app;
		Authentication::logout();
		$app->flash('info', "Aurevoir.");
		$app->redirectTo('root');
	 } 
	 
	 public function solidComment() {
		 AnonymousController::verifConnexion();
		$app = Controller::$app;
		$commentaire = $app->request->post('commentaire');
		$commentaire = filter_var($commentaire, FILTER_SANITIZE_STRING);
		
		if (trim($commentaire) === "") {
			$app->flash('info', "Le contenu du commentaire ne peut être vide!<br>");
		}
		
		// On s'assure que l'utilisateur n'a rien fait de malhonnête dans le cookie
		$idBillet = filter_var($_COOKIE['dernierePage'],FILTER_SANITIZE_NUMBER_INT);
		setcookie('idUser',null);
		$idUsr = $_SESSION['id'];
		
		$nvCommentaire =new Commentaires();
		$nvCommentaire->message=$commentaire;
		$nvCommentaire->id_billet=$idBillet;
		$nvCommentaire->id_utilisateur=$idUsr;
		$nvCommentaire->save();
		$app->flash('info', "Ce commentaire lumineux à été ajouté.<br>");
		$app->redirectTo('root');
	 }
	 
	 // saisir un billet
	 public function formBillet() {
		 AnonymousController::header();
		 $categories = Categories::all();
		 $labels = array();
		 foreach ($categories as $catTmp) {
			 array_push($labels,$catTmp->label);
		 }
		 Controller::$app->render('redacBillet.php',compact('labels'));
	 }
	 
	 public function solidBillet() {
		AnonymousController::verifConnexion(); // On s'assure par la même ocacsion que l'utilisateur n'est pas radié...
		AnonymousController::header();
		$app = Controller::$app;
		$billet = $app->request->post('billet');
		$billet = filter_var($billet, FILTER_SANITIZE_STRING);
		$titre = $app->request->post('titre');
		$titre = filter_var($titre, FILTER_SANITIZE_STRING);
		
		if (trim($titre) === "") {
			$app->flash('info', "Le titre ne peut être vide.<br>");
			$app->redirectTo('root');
		}
		if (trim($billet) === "") {
			$app->flash('info', "Le message ne peut être vide.<br>");
			$app->redirectTo('root');
		}
		$nvBillet = new Billets();
		$nvBillet->message=$billet;
		$nvBillet->titre=$titre;
		
		$nvBillet->id_utilisateur = $_SESSION['id'];
		$nvBillet->id_categorie = $app->request->post('selectCategorie');
		$nvBillet->save();
		
		$app->flash('info', "Votre message a été ajouté!<br>");
		$app->redirectTo('root');
	 }
	 
	 public function formSettings() {
		AnonymousController::header();
		Controller::$app->render('changesettings.php');
	 }
	 
	 public function solidSettings() {
		 
		AnonymousController::verifConnexion();
		$changement = false;
		
		$app = Controller::$app;
		// Vérification: Pour voir si rien n'a été changé (active un boolean)
		$allPostVars = $app->request->post();
		$utilisateur = Utilisateurs::find($_SESSION['id']);
		
		foreach($allPostVars as $cle => $element) {
			$element = trim($element); // On s'assure qu'on ait pas de chaînes de caractères vide
			 
			if ($element !== "" && $element  !== "Go go go!") { // Il faut virer le gogogo qui compte comme un élément...
				// svg de $element dans une de mes variables..
				// On doit virer le gogo qui correspond au bouton...
				$changement = true;
				 switch ($cle) {
					 case "newName":
						$newName = filter_var($element, FILTER_SANITIZE_STRING);
						$utilisateur->nom = $element;
						break;
					case "newFirstName":
						$element = filter_var($element, FILTER_SANITIZE_STRING);
						$utilisateur->prenom = $element;						
						break;
					case "newMail":
						if (filter_var($element, FILTER_VALIDATE_EMAIL)) { // supprime l'email
							$element = filter_var($element, FILTER_SANITIZE_EMAIL);
						}
						$utilisateur->mail = $element;
						break;
					case "newMdp":
						$element = filter_var($element, FILTER_SANITIZE_STRING);
						$utilisateur->mdp = password_hash($element, PASSWORD_BCRYPT);	
						break;
				 }
						$utilisateur->save();
			}
		}
		
		if (!$changement) {
			$app->flash('info', "Vous n'avez rien changé.<br>");
			$app->redirectTo('root');
		}
		else {
			// Pas de flash, car la méthode de déco en envoie un.
			$this->deconnexion();
		}
	 }
}
