<?php
use conf\Authentication;
use app\model\Utilisateurs;
/** Réservé aux meilleurs des meilleurs
 * Peut avoir accès à la liste des membres (et les radier)
 * Ajouter des catégories de blog
 * */
class AdminController extends Controller {
	public function afficheMembres() {	
		AnonymousController::header();
		$id = array();
		$pseudos = array();
		$radie = array();
		
		$allUsr  = Utilisateurs::all();
		foreach ($allUsr as $u) {
			array_push($id,$u->id);
			array_push($pseudos,$u->pseudo);
			array_push($radie,$u->radie);
		}	
		
		Controller::$app->render('listeMembre.php',compact('id','pseudos','radie'));
	}
	
	public function solidRadiation(){
		AnonymousController::header();
		// Permet la radiation effective d'un membre
		// Comment retrouver la valeur du bouton qui a envoyé l'ordre
		$app = Controller::$app;
		$allUsr = Utilisateurs::all();
		$i = 0;
		foreach ($allUsr as $u) {
			$nvlRadie = $app->request->post("user$i");
			$u->radie = $nvlRadie;
			$u->update();
			$i++;
		}
		$app->flash('info', "Vos modifications ont été prises en compte <br>");
		$app->redirectTo('root');
	}
	
	public function formCategorie() {
		AnonymousController::header();
		Controller::$app->render('ajouteCategorie.php');
	}
	
	public function solidCategorie() {
		$app = Controller::$app;
		AnonymousController::header();
		$nouvelleCat = $app->request->post('nvlCategorie');
		$nvlCat = new Categories();
		$nvlCat->label = $nouvelleCat;
		
		// Vérification d'un potentiel doublon
		$allCategories = Categories::all();
		$doublon = false;
		foreach ($allCategories as $cat) {
			if ($cat->label == $nouvelleCat) {
				$doublon = true;
			}
		}
		
		if (!$doublon) {
			$app->flash('info', "Ajout de la nouvelle catégorie '$nouvelleCat'<br>"); 
			$nvlCat->save();
		}
		else $app->flash('info', "Cette catégorie existe déjà! <br>");
		$app->redirectTo('root');
		
	}
		
}
