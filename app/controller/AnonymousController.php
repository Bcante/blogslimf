<?php
use conf\Authentication;
use app\model\Utilisateurs;
use Illuminate\Database\Capsule\Manager as DB;
require('vendor/autoload.php');
// Avec ce require je ne suis pas sensé avoir de problèmes pour avoir les modèles...

class AnonymousController extends Controller {


    public static function header() {
		$app = Controller::$app;
		/** Pour le moment on ne teste que si il y a une var de sesssion, dans le futur il faut tester la valeur de credential **/
		if (!isset($_SESSION['lvlAcces'])){
				$app->render('anonHeader.php',compact('app'));
			}
		else if ($_SESSION['lvlAcces'] === 0) {
			$app->render('userHeader.php',compact('app','utilisateurs')); // Il faut imaginer que utilisataurs et app sont précédés d'un $. On pourra l'utiliser dans la vue.
		}
		else {
			$app->render('adminHeader.php',compact('app'));
		}
	}

    public function footer() {
		if (isset($_COOKIE['pseudoUsr'])) {
			if (isset($_SESSION['lvlAcces'])) {
				if ($_SESSION['lvlAcces']===1) {
					Controller::$app->render('adminFooter.php');
				}	
			}
		Controller::$app->render('footer.php');
		}
    }

    public function index(){
		$categories = Categories::all();
		 $labels = array();
		 foreach ($categories as $catTmp) {
			 array_push($labels,$catTmp->label);
		}
		// Vérifiation si on a rien dans les var de session, et un cookie en attente.
		$app = Controller::$app;
		$this->header();
		Controller::$app->render('ligneAddBillet.php');
		Controller::$app->render('lienPageDonnee.php',compact('labels'));
		Controller::$app->render('lienInfo3D.php');
		$this->footer();
    }
    
    public function aff_page($id) {
		
		if (isset ($_COOKIE['categorieRecherche'])) {
			$categorie = $_COOKIE['categorieRecherche'] +1;
		}
		else $categorie = -1;
			
		 $categories = Categories::all();
		 $labels = array();
		 foreach ($categories as $catTmp) {
			 array_push($labels,$catTmp->label);
		 }
		 
		$app = Controller::$app;
		$this->header();
		if ($categorie != -1) {
			$countRequirement = ['blog_utilisateurs.radie' => 0, 'blog_billets.id_categorie' => $categorie];
			$totalBillets = DB::table('blog_utilisateurs')->join('blog_billets', 'blog_utilisateurs.id', '=', 'blog_billets.id_utilisateur')
													 	->where($countRequirement)
														->count();
		}
		else {
			$totalBillets = DB::table('blog_utilisateurs')->join('blog_billets', 'blog_utilisateurs.id', '=', 'blog_billets.id_utilisateur')
													 	->where('blog_utilisateurs.radie', '=' , 0)
														->count();
														
		}
		
		$whereRequirement = ['blog_utilisateurs.radie' => 0, 'blog_billets.id' => $id];
		$borneMax = $id*20; // Si j'ai la page 1 j'affiche le billet 1 a 20.
		$borneMin = $borneMax-20;
		$nbPage = $totalBillets / 20;
		if ($totalBillets == 0) {
			$app->flash('info', "($categorie)Il n'existe pas encore de billets pour cette catégorie");
			 
			$app->redirectTo('root'); 
		}
		
		if ($borneMax > $totalBillets) {
			$borneMax = $totalBillets; //(pour éviter le stack oferflow)
		}
		
		if ($borneMin > $borneMax || $id == 0) {
			$app->flash('info', "Cette page n'existe pas."); 
			$app->redirectTo('root'); 
		}
		
		else {
			
			if ($categorie != -1) {
			$displayRequirement = ['blog_utilisateurs.radie' => 0, 'blog_billets.id_categorie' => $categorie];
			$list = DB::table('blog_utilisateurs')->join('blog_billets', 'blog_utilisateurs.id', '=', 'blog_billets.id_utilisateur')
													->where($displayRequirement)
													->orderBy('blog_billets.date', 'DESC')
													->skip($borneMin)
													->take(20) // Pas besoin de filtrer le take, il s'arete tout seul
													->get();
			}
			else {
				$displayRequirement = ['blog_utilisateurs.radie' => 0];
				$list = DB::table('blog_utilisateurs')->join('blog_billets', 'blog_utilisateurs.id', '=', 'blog_billets.id_utilisateur')
													->where($displayRequirement)
													->orderBy('blog_billets.date', 'DESC')
													->skip($borneMin)
													->take(20) // Pas besoin de filtrer le take, il s'arete tout seul
													->get();
			}
			Controller::$app->render('pageCible.php', compact('totalBillets','list','nbPage','labels'));
		}
		
	}
    
    public function affiche_billet($id) {
		$this->header();
		
		$billetCible = Billets::find($id);
		$author = Utilisateurs::find(($billetCible->id_utilisateur))->pseudo;
		
		if (Utilisateurs::find(($billetCible->id_utilisateur))->radie == 1) {
			echo "Le billet que vous essayez d'afficher à été écrit par un utilisateur radié depuis.";
		}
		else {
			// Affiche le billet si on a le droit
			Controller::$app->render('billetCible.php', compact('billetCible','author'));
			
			/** Affichage du fil de commentaire **/
			$whereRequirement = ['blog_utilisateurs.radie' => 0, 'blog_billets.id' => $id];
			$tousCommentairesv2 = DB::table('blog_commentaires')
									->join('blog_billets', 'blog_billets.id', '=' , 'blog_commentaires.id_billet')
									->join('blog_utilisateurs', 'blog_commentaires.id_utilisateur', '=' , 'blog_utilisateurs.id')
									->where($whereRequirement)
									->select('blog_commentaires.message','blog_utilisateurs.pseudo', 'blog_commentaires.date')
									->orderBy('blog_commentaires.date', 'DESC')
									->get();
									
			Controller::$app->render('commentairesBilletCible.php', compact('tousCommentairesv2','clefEtrangeres'));

			$this->footer();
			if (isset($_SESSION['pseudo'])) { // ça ne sert à rien d'enregistre un cookie pour un utilisateur qui ne peut commenter
				setcookie("dernierePage", $id ,time() + 60*60*24*7); 
				Controller::$app->render('addCommentaire.php');
			}
		}
	}
    
    /** Affiche le formulaire d'inscription ainsi que le header de la page.
     * */
    public function inscritMembre() {
		$this->header();
		Controller::$app->render('form_insc.php');
	}
	
	public function solidInscription() {
		/** Faut il envoyer chier l'utilisateur qui met des balises? **/
		$app = Controller::$app;
				
		$nom = $app->request->post('nom');
		$prenom = $app->request->post('prenom');
		$mail = $app->request->post('mail');
		$mdp = $app->request->post('mdp');
		$pseudo = $app->request->post('pseudo');
		
		$code = Authentication::inscription($nom,$prenom,$mail,$mdp,$pseudo);
		
		switch($code) {
			case 0:
				$app->flash('info', "Vous êtes désormais inscris!"); 
				$app->redirectTo('root'); 
				break;
			
			case 1:
				$app->flash('info', "Tous les champs doivent être remplis (et pas uniquement avec des espaces)"); 
				$app->redirectTo('forminscription'); // Utilisation d'un redirect plutôt qu'une fonction pour que le flash apparaisse
				break;
				
			case 2:
				$app->flash('info', "Merci de respecter les indications liées au mot de passe"); 
				$app->redirectTo('forminscription'); // Utilisation d'un redirect plutôt qu'une fonction pour que le flash apparaisse
				break;
				
			case 3:
				$app->flash('info', "Ce pseudo est déjà utilisé"); 
				$app->redirectTo('forminscription'); // Utilisation d'un redirect plutôt qu'une fonction pour que le flash apparaisse
				break;
			}
		}
		
	public function solidConnexion() {
		
		$app = Controller::$app;
		
		$pseudo = $app->request->post('pseudo');
			$pseudo = filter_var($pseudo, FILTER_SANITIZE_STRING);
		
		$mdp = $app->request->post('mdp');
			$mdp = filter_var($mdp, FILTER_SANITIZE_STRING);
		
		/** On fait un switch sur le retour d'authenticate **/
		
		$code = Authentication::authenticate($pseudo, $mdp);
		
		switch ($code) {
			case 0:
				$app->flash('info', "Bonjour !"); 
				$app->redirectTo('root');
				break;
			case 1:
				$app->flash('info', "Informations de connexion incorrectes."); 
				$app->redirectTo('portail');
				break;
			case 2:
				$app->flash('info', "Votre compte est radié. Vous ne pouvez plus vous connecter."); 
				$app->redirectTo('root');
				break;
		}
		
	}
	
	/** Fonction générique pour vérifier à tout moment si on est connecté **/
	public static function verifConnexion() {
		$code = Authentication::isAuthenticated();
		$app = Controller::$app;
		
		switch ($code) {
			case 1: // Cookie en vie, session expirée
				$app = Controller::$app;
				$app->flash('info', "Votre session a expiré. Merci de vous reconnecter."); 
				$app->redirectTo('portail');
				break;
			case 2:
				Authentication::logout(); // L'utilisateur n'est pas la bienvenue...
				$app->flash('info', "Votre compte est radié. Vous ne pouvez plus vous connecter."); 
				$app->redirectTo('root');
				break;
		}
	}
	
	public function infoCategories(){
		$app = Controller::$app;
		$this->header();
		$donnesCategories = array();
		
		$nbCategorie = Categories::all()->count();
		//echo "Nombre de catégories: ".$nbCategorie;
		
		for ($i = 1 ; $i < $nbCategorie+1 ; $i++) {
			$labelCatTmp = DB::table('blog_categories')->join('blog_billets', 'blog_categories.id', '=', 'blog_billets.id_categorie')
														->where('blog_categories.id', '=' , $i)
														->select('blog_categories.label')
														->get();		
		$label = (Categories::find($i)->label);			
		$donnesCategories[$label] = sizeof($labelCatTmp);
		}
											
		$app->render('info3d.php',compact('donnesCategories'));
	}
	
	public function portail(){
		$this->header();
		Controller::$app->render('log.php');
	}
	
	public function changeCat() {
		$app = Controller::$app;
		$cat = $app->request->get('selectCategorie');
	
		setcookie('categorieRecherche',$cat);
		
		$this->index();
	}
	
	public function resetFilter() {
		setcookie('categorieRecherche',null);
		$this->index();
	}
}

?>
