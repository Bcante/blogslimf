<?php

namespace conf;
use app\model\Utilisateurs;

//require('vendor/autoload.php');

class Authentication {

    public static function authenticate($pseudo, $passUser) {
		$e = new Utilisateurs();
		$allUser = Utilisateurs::all();
		$mdpHash = "";
		$usrMatch;
		$connexionOK = false;
		$utilisateurAdmis = false;
		$codeRetour = 1;
		
		$res = false;
		
		foreach ($allUser as $usrTmp) {
			if ($usrTmp->pseudo === $pseudo) {
				$usrMatch = $usrTmp;
				$mdpHash = $usrMatch->mdp;
			}
		}
		if (!isset($usrMatch)) {
			$connexionOK = false;
		}
			// On isole le hash de la personne qui nous intéresse...
		$passSale = $passUser.'sel';
		
		if (password_verify($passSale, $mdpHash)) {
			$connexionOK = true;
			// On a le bon mdp pour le bon utilisateur
		}
		
		/** Les informations de connexion sont bonnes: On vérifie
		 * maintenant que ce n'est pas un utilisateur radié.
		 * **/
		if ($connexionOK) {
			if ($usrMatch->radie == 0) { // On teste si l'utilisateur à le droit de se co...
				$utilisateurAdmis = true;
				
			}
			else $codeRetour = 2;
		}
			
		if ($utilisateurAdmis) { // Positionnement var session & cookie
			session_destroy(); // session_regenerate_id ?
			session_start();
			$profil = $usrMatch->profil;
			// On détruit tout ce qu'il y a dedans et on recommence
	
			//Ces informations doivent être soigneusement salées etc ...
			$idUsr = $usrMatch->id;
			setcookie("pseudoUsr", $pseudo ,time() + 60*60*24*7); // Servira de 'clef' pour accèder aux variables de sessions
			if ($profil === "admin") {
				$_SESSION['lvlAcces'] = 1; // le lvl d'accès de la BDD,  membre 1 admin
			}
			else $_SESSION['lvlAcces'] = 0;
			$_SESSION['pseudo'] = $pseudo; 
			
			$_SESSION['id'] = $idUsr;
			$codeRetour = 0;
		}
		return $codeRetour;
    }


    public static function logout() {
		session_destroy();
		setcookie('pseudoUsr',null);
		setcookie('dernierePage',null);
		setcookie('pseudoUsr',null);
		setcookie('categorieRecherche',null);
		/** Pour tous les cookies crées : setCookie('nom',null);**/
		
		session_start(); // On recommence une session à zéro. 
    }

	/** Permet de vérifier si l'utilisateur est enregistré.
	 * Renvoie 0 si tout va bien, 1 si l'utilisateur doit se reconnecter, 2 si l'utilisateur est
	 * banni et ne peut donc se reconnecter **/

    public static function isAuthenticated() {
		$isAuth = 0;
		// Vérifiation si on a rien dans les var de session, et un cookie en attente.
		if (!isset($_SESSION['pseudo']) ) {
			$isAuth = 1;
		}
		else {
			// On s'assure au passage que l'utilisateur n'est pas radié
			$e = Utilisateurs::find($_SESSION['id']);
			if ($e->radie == 1)
			$isAuth = 2;
		}
		return $isAuth;
	}

    public static function getUser() {
	// More code needed here
    }
    
    /** Permet l'inscription (ou non) d'un membre
     * Renvoie 0 si tout va bien, 1 si un des champs est vide;
     * 2 si le mdp ne respecte pas la politique de sécurité
     * et 3 si pseudo déjà utilisé **/ 
    public static function inscription($nom, $prenom, $mail, $mdp, $pseudo) {
		$codeRetour = 0;
		
		$formulaireOk = true;
		$inscriptionComplete = false;
		$nom = filter_var($nom, FILTER_SANITIZE_STRING);
			if (trim($nom) === "") $codeRetour = 1;
			
		$prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
			if (trim($prenom) === "") $codeRetour = 1;
			
		filter_var($mail, FILTER_VALIDATE_EMAIL);
			$mail = filter_var($mail, FILTER_SANITIZE_EMAIL);
			if (trim($mail) === "") $codeRetour = 1;
			
		$mdp = trim($mdp);
		$mdp = filter_var($mdp, FILTER_SANITIZE_STRING); // Cette ligne va hasher le mot de passe. 
			if ($mdp === "") $codeRetour = 1;
			
		$mdpSale = $mdp.'sel';	
		$mdphash = password_hash($mdpSale, PASSWORD_BCRYPT);
			
		$pseudo = filter_var($pseudo, FILTER_SANITIZE_STRING);
			if (trim($pseudo) === "") $codeRetour = 1;	
			
			
		// on Vérifie qu'on ai pas un champ vide
		// et que c'est utile de continuer		
		if ($codeRetour == 0) {
			$policeMdp = Authentication::passPolicy($mdp);	
			if ($policeMdp === false) $codeRetour = 2;
		}
		
		// on vérifie que le mot de passe n'est pas déjà utilisé
		// pour ne pas provoquer une erreur slim.
		if ($codeRetour == 0) {
			$usrExistant = Utilisateurs::where('pseudo','=',$pseudo)->get()->count();
			if ($usrExistant === 1) 
				$codeRetour = 3;
		}
			
		
		if ($codeRetour == 0) {
			$e = new Utilisateurs();
			$e->pseudo = $pseudo;
			$e->mdp = $mdphash;
			$e->nom = $nom;
			$e->prenom = $prenom;
			$e->mail = $mail;
			$e->save();
			$inscriptionComplete = true;
		}	
		
		return $codeRetour;
	}
	
	/** Création maison d'une, politique de mot de passe: Il faut au moins une maj, un chiffre,
	 * et que la taille du mdp soit supérieur à 7 caractères.
	 * Le MDP est déjà 'trim' .**/
	public function passPolicy($mdp){
		$arrMdp = str_split($mdp);
		$tailleOk = false;
		$uneMaj = false;
		$unChiffre = false;
		// On vérifie par la magie des expressions régulières si il y a une maj, et un chiffre
		if (preg_match('/[0-9]/',$mdp)) 
			$unChiffre = true; 
		if (preg_match('/[A-Z]/',$mdp)) ;
			$uneMaj = true;
		if (strlen($mdp) > 7)
			$tailleOk = true;
		
		if ($tailleOk && $uneMaj && $unChiffre) {
			return true;
		}
		else return false;
	}
		
    // More methods needed here
}
